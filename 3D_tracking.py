import numpy as np
import cv2
import matplotlib.pyplot as plt


def show_z(x_valmin,y_valmin,x_valmax,y_valmax):
    
    heightOfCamera = 77;    #adjust in cm, according to your height from the floor
    focalLength = 540;
    armsize = 10;
    #values = image[x_val,y_val];

    x_diff = x_valmax - x_valmin;
    y_diff = y_valmax - y_valmin;
    arm_length = (x_diff*x_diff + y_diff*y_diff)**(0.5);
    
    if arm_length==0:
        return 0
    z_val = heightOfCamera - ((focalLength*armsize)/arm_length);
    
    z_val = int(z_val)
    return z_val;

def find_centroid(kp,x_old,y_old):
    if len(kp)==0:
        return 0,0,0,0,0,0,''  
          
    x_cord = 0
    y_cord = 0
    x_arr=[]
    y_arr=[]

    # loop over the keypoints
    for i in range(len(kp)):
        x_cord += kp[i].pt[0]    
        x_arr.append(kp[i].pt[0])
        
        y_cord += kp[i].pt[1]
        y_arr.append(kp[i].pt[1])      
        
        
    x_center = int(x_cord/len(kp))
    y_center = int(y_cord/len(kp))

    #print str(x_center)+" "+str(x_old)+" is x"
    #print str(y_center)+" "+str(y_old)+" is y"
    return_dir = ""
    if(y_center<y_old):
        return_dir = return_dir + "down "
        if(x_center <x_old):
            return_dir = return_dir + "left"
        elif(x_center > x_old):
            return_dir = return_dir + "right"
        else:
            return_dir = return_dir + ""
    elif(y_center>y_old):
        return_dir = return_dir + "up "
        if(x_center <x_old):
            return_dir = return_dir + "left"
        elif(x_center > x_old):
            return_dir = return_dir + "right"
        else:
            return_dir = return_dir + ""
    else:
        return_dir = return_dir + ""
        if(x_center <x_old):
            return_dir = return_dir + "left"
        elif(x_center > x_old):
            return_dir = return_dir + "right"
        else:
            return_dir = return_dir + "center"

    x_min,min_idx = min( (x_arr[i],i) for i in xrange(len(x_arr)))
    x_max,max_idx = max( (x_arr[i],i) for i in xrange(len(x_arr)))

    y_min = y_arr[min_idx]
    y_max = y_arr[max_idx]


    return x_center,y_center,x_min,x_max,y_min,y_max,return_dir
 

def edge_detector(frame,count,x_val,y_val):
    fast = cv2.FastFeatureDetector(35)
    img = frame
    kp = fast.detect(img,None)
    img2 = cv2.drawKeypoints(img, kp, color=(255,0,0))
    #font = cv2.FONT_HERSHEY_SIMPLEX

    x_val, y_val,x_valmin,x_valmax,y_valmin,y_valmax,return_dir = find_centroid(kp,x_val,y_val)
    z_val = show_z(x_valmin,y_valmin,x_valmax,y_valmax)
    text_centroid = 'x = '+str(x_val)+' y = '+str(y_val)+' z = '+str(z_val)+' direction is '+return_dir
    #print text_centroid + str(count)
    #cv2.circle(img2,(int(x_valmin),int(y_valmin)),10,(0,0,255),-1)
    #cv2.circle(img2,(int(x_valmax),int(y_valmax)),10,(0,0,255),-1)
    cv2.circle(img2,(x_val,y_val),10,(0,0,255),-1)
    cv2.putText(img2,text_centroid,(0,440),2,0.75,(255,0,255),1,1)
    cv2.imshow('edge',img2)
    
    return x_val,y_val;

cap = cv2.VideoCapture(0)       #change to 1 , if you have web camera on your system
count = 0;
x_val = 0;
y_val = 0;
while(cap.isOpened()):
    ret, frame = cap.read()
    if ret==True:
    	x_val,y_val = edge_detector(frame,count,x_val,y_val)
	count = count + 1
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break

cap.release()
