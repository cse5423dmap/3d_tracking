#3D tracking a Quadcopter

The goal of this project is to track the movement of a mini drone or a mini quadcopter, using a basic USB webcam. In this particular project, we have used a Logitech QuickCam® Orbit AF and a Crazyflie 2.0 mini quadcopter by Bitcraze. 

----------

## Getting Started

The very first step to get this system setup on your local machine would be to use the following Git command to clone the repository on your local disk. 

    git clone https://bitbucket.org/cse5423dmap/3d_tracking.git

Alternatively, you can also download the .zip file onto your local disk and extract the files in one location. The **production** branch contains the code that is ready for deployment. 

Once the code is downloaded you may need to check the requirements given below and update your system accordingly. 

### Prerequisites
This program has the following software requirements: 

* Python  2.7.12
	* Numpy 
	* Matplotlib
	* OpenCV 2.4.X

In case you need help in setting up the above given dependencies, kindly follow the steps provided in section below. 

In terms of Hardware requirements, this program will just need a basic USB camera or the laptop camera as well. We used the Logitech QuickCam® Orbit AF camera. However in essence, any webcam can be used for this project, so long as python detects the camera. 

####Camera test

In order to check whether your webcam is detected by python or not, kindly try the test code provided in the repository. 

The file is named ***Webcam_test.py*** under the **test** folder. 

Open the command prompt on your system, navigate to the file's location and then run the following command 

    python Webcam_test.py

You should see a window that opens up reading the scene in black and white. This is just for testing the camera, no video is actually recorded.  Press **q** to exit the program. 

*In case you are using a laptop and the USB camera is attached and still the default laptop webcam pops up, go to the test file and make a small change in the third line of the code, swap the argument of the **Video_capture** function from 0 to 1 , or vice versa, this should switch between the two cameras.* 

    cap = cv2.VideoCapture(1)


### Installation

First, we will install the requirements. we need the python version 2.7.x for this program. The download and the installation steps can be followed from the official python website. Choose the setup according to the local OS used in your machine. 

`https://www.python.org/downloads/`

Once python 2.7 or higher has been installed, the next step would be to get **OpenCV**, but before we get that, we need to get **Numpy** and **Matplotlib** as well. 

In order to install Numpy, you can simply run the following command in Windows: 

    pip install numpy

In case you do not have pip installed in your machine, you can use the steps provided in the following link:

    http://scipy.org/install.html

The above link provides downloads for all the different OSes and the corresponding steps to install them as well. 

Once Numpy has been installed, we can go ahead and install matplotlib. In order to do that, use the following command:

    pip install matplotlib

Again, if you do not have **pip** installed, you can get the package from the following link. 

    http://matplotlib.org/users/installing.html

This link gives the steps for all the different OSes. 

The last requirement would be OpenCV package. 

The pip command is given as follows: 

    pip install opencv-python

Or use the steps in the link below, according to your OS: 

***Windows:*** 

    http://docs.opencv.org/2.4/doc/tutorials/introduction/windows_install/windows_install.html

***Ubuntu:***
  
      http://docs.opencv.org/2.4/doc/tutorials/introduction/linux_install/linux_install.html
    


##Deployment 

After the above given prerequisites have been installed and the camera has been tested, you can go ahead with the deployment of the program. In order to use the program, we would have to setup the camera in such a way that the background of the video , would have to be as plain as possible.  A background with too much clutter and noise would not allow the features of the drone to be extracted properly. 

In our case, we used the camera in such a way that the lens pointed down towards the floor and on the floor we placed a piece of cardboard such that it would hide the carpet, at an ideal distance of 66.2 cm (30 in).

Please refer to the screenshot below: 

![enter image description here](http://jayneesh.me/demo/Screenshot_2.jpg)

As you can see the camera is pointing downwards and the piece of cardboard is used to give a stable background for the video. 

Once this setup is done, open command prompt, navigate to the folder where the file **3D_tracking.py** exists.

And then enter the following command: 

    python 3D_tracking.py 

A new window will come up that will be a live feed of your webcam. In this window, you will be able to see the X,Y and Z coordinates of the drone. The video will shows small blue dots for the extracted features and edges of the drone and a bright red dot denoting the centroid of these features. 


You can now fly the drone around and see the changes in the values as it is tracked. Please refer to the screenshots, given in the following section. 


## Contributors 

Abhishek Aravind Kulkarni - ak229@buffalo[dot]edu

Jayneesh Haresh Wanjara - jayneesh@buffalo[dot]edu

Pratik Vijay Patil - Pratikvi@buffalo[dot]edu

Please reach out to us in case you have any queries. 

## References

[1] http://www.pyimagesearch.com/2015/01/19/find-distance-camera-objectmarker-using-python-opencv/

[2] http://www.pyimagesearch.com/2014/09/29/finding-brightest-spot-image-using-python-opencv/ 

[3] http://stackoverflow.com/questions/32943227/python-opencv-capture-images-from-webcam

[4]http://www.bogotobogo.com/python/OpenCV_Python/python_opencv3_basic_image_operations_pixel_access_image_load.php

[5] http://docs.opencv.org/2.4/modules/highgui/doc/reading_and_writing_images_and_video.html

[6] http://docs.opencv.org/2.4/doc/tutorials/introduction/display_image/display_image.html

[7] http://www.pyimagesearch.com/2015/08/24/resolved-matplotlib-figures-not-showing-up-or-displaying/



